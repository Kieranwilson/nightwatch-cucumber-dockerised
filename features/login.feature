Feature: Google Search

Scenario: Verifying the search input is on Google

  Given I open Google`s search page
  Then the title is "Google"
  And the Google search form exists

Scenario: Searching Google

  Given I open Google`s search page
  When I search for "pigeons"
  Then I expect to see a Wikipedia entry
