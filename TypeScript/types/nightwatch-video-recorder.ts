declare module 'nightwatch-video-recorder' {
    export function start(config: Object, callBack: Function): Promise<void>;
    export function stop(config: Object, callBack: Function): Promise<void>;
}
