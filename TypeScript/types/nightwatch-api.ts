declare module 'nightwatch-api' {
    import { NightwatchAPI } from "nightwatch";
    export const client: NightwatchAPI;
    export function createSession(testSettings: String): Promise<NightwatchAPI>;
    export function closeSession(): Promise<void>;
}