import {
  After,
  AfterAll,
  Before,
  BeforeAll,
  setDefaultTimeout,
  World
} from "cucumber";
import { client, closeSession, createSession } from "nightwatch-api";
import { start, stop } from "nightwatch-video-recorder";

let browserObj: Object;

const buildVideoObject = (testCase: World, prevObject: any = {}): Object => ({
  ...prevObject,
  currentTest: {
    module: testCase.pickle.name.replace(/ /g, "_"),
    result: { failed: !!testCase.result && testCase.result.status === "failed" }
  },
  globals: { test_settings: client.session().options }
});

const dateTimeString = (): String =>
  new Date()
    .toISOString()
    .split(".")[0]
    .replace(/:/g, "-");
const buildScreenshotPath = (testCase: World) =>
  `${client.session().options.screenshotsPath}/${
    testCase.pickle.name
  }-${dateTimeString()}.png`;

setDefaultTimeout(60000);

BeforeAll(async () => {
  await createSession("default");
});

AfterAll(async () => {
  await closeSession();
});

Before((testCase: World, callback: Function) => {
  browserObj = buildVideoObject(testCase);
  start(browserObj, callback);
});

// Asynchronous Promise
After((testCase: World, callback: Function) => {
  browserObj = buildVideoObject(testCase, browserObj);
  client.saveScreenshot(buildScreenshotPath(testCase));
  stop(browserObj, callback);
});
