import { Given, Then, When } from "cucumber";
import { client } from "nightwatch-api";

Given(/^I open Google`s search page$/, async () => {
  await client.url("http://google.com");
});

Then(/^the title is "(.*?)"$/, async text => {
  await client.assert.title(text);
});

Then(/^the Google search form exists$/, async () => {
  await client.expect.element('input[name="q"]').to.be.visible;
});

When(/^I search for "(.*?)"$/, async text => {
  await client
    .setValue('input[name="q"]', text)
    .click('input[type="submit"]')
    .waitForElementVisible("#search", 5000);
});

Then(/^I expect to see a Wikipedia entry$/, async () => {
  await client.expect
    .element("body")
    .text.to.contain("https://en.wikipedia.org");
});
