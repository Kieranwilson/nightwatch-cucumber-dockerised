# Nightwatch with cucumber

This is just an example of using feature based cucumber testing written with Node.
It is supported by Nightwatch and nightwatch-api to provide the functionality.

It also uses a little bit of TypeScript to help with function autocompletion and to basically
help ensure I am using nightwatch correctly.

## Getting Started

```
yarn install
yarn build
docker-compose run --rm nightwatch
```

It will add screenshots to `/env/screenshots` and videos to `/env/videos`
